#!/usr/bin/python3

# Importing the Keras libraries and packages
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.models import load_model

import tensorflow as tf
import sys

# Archivo
archivo=sys.argv[1]

if not archivo:
    print("Falta archivo a verificar.", file=sys.stderr)
    sys.exit(2)

# Profiling
# NameError: name 'run_metadata' is not defined
#from tensorflow.python.client import timeline
#trace = timeline.Timeline(step_stats=run_metadata.step_stats)
#trace_file = open('timeline.ctf.json', 'w')
#trace_file.write(trace.generate_chrome_trace_format())

# Multicore session
from keras.backend import tensorflow_backend as K
with tf.Session(config=tf.ConfigProto(
        device_count={"CPU": 8},
        inter_op_parallelism_threads=8,
        intra_op_parallelism_threads=7,
        log_device_placement=False)) as sess:

    K.set_session(sess)

    # Initialising the CNN
    classifier = Sequential()

    # Step 1 - Convolution
    classifier.add(Conv2D(32, (3, 3), input_shape = (640, 480, 3), activation = 'relu'))

    # Step 2 - Pooling
    classifier.add(MaxPooling2D(pool_size = (2, 2)))

    # Adding a second convolutional layer
    classifier.add(Conv2D(32, (3, 3), activation = 'relu'))
    classifier.add(MaxPooling2D(pool_size = (2, 2)))

    # Step 3 - Flattening
    classifier.add(Flatten())

    # Step 4 - Full connection
    classifier.add(Dense(units = 128, activation = 'relu'))
    classifier.add(Dense(units = 1, activation = 'sigmoid'))

    # Compiling the CNN
    classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
    classifier = load_model('noise.hdf5')

    # Part 3 - Making new predictions
    import numpy as np
    from keras.preprocessing import image
    test_image = image.load_img(archivo, target_size = (640, 480))
    test_image = image.img_to_array(test_image)
    test_image = np.expand_dims(test_image, axis = 0)
    result = classifier.predict(test_image)
    #training_set.class_indices

    if result[0][0] == 0:
        prediction = 'con ruido'
    else:
        prediction = 'sin ruido'

    print("Archivo: "+ archivo + "  ------------  Prediccion ==> " + prediction)