#!/bin/bash

filename=$(basename -- "$1")
extension="${filename##*.}"
filename="${filename%.*}"

convert $1 -size 1024x768 +noise Impulse training_noise/${filename}_impulse.$extension &
convert $1 -size 1024x768 +noise Gaussian training_noise/${filename}_gaussian.$extension 


